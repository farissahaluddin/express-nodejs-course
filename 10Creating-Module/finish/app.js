var flight = require('./flight');

flight.setOrigin('Kensington');
flight.setDestination('london');
flight.setNumber('345');

console.log(flight.getInfo());

var anotherFlight = require('./flight');

console.log(anotherFlight.getInfo());